local reload_period = 1
local time_left = reload_period + 10
local load_radius = 16

minetest.register_globalstep(function(dtime)
	time_left = time_left - dtime
	if time_left < 0 then
		time_left = reload_period
		for _, player in ipairs(minetest.get_connected_players()) do
			minetest.get_voxel_manip(vector.subtract(player:get_pos(), load_radius), vector.add(player:get_pos(), load_radius))
		end
	end
end)
